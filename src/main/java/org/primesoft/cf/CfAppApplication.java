package org.primesoft.cf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableJpaRepositories
@EnableEurekaClient
public class CfAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(CfAppApplication.class, args);
    }
        @Bean
        @LoadBalanced
 	   public RestTemplate getRestTemplate() {
 	      return new RestTemplate();
 	   }
    

}
