package org.primesoft.cf.repository;

import java.util.UUID;

import org.primesoft.cf.entity.AccountDetail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface AccountDetailsRepository extends CrudRepository<AccountDetail, UUID> {

}
