package org.primesoft.cf.repository;

import java.util.List;
import java.util.UUID;

import org.primesoft.cf.entity.Forecast;
import org.primesoft.cf.entity.ForecastTransaction;
import org.primesoft.cf.entity.Transaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ForecastTransactionRepository extends CrudRepository<ForecastTransaction, UUID> {
	 List<ForecastTransaction> findByForecast(Forecast forecast);
	 List<ForecastTransaction> findByTransaction(Transaction transaction);
	}
